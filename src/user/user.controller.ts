import { Controller, Request, Post, Body, HttpException, HttpStatus, HttpCode } from '@nestjs/common';
import { UserDto } from './dto/user.dto';
import { AuthService } from '../auth/auth.service';
import { ApiUseTags } from '@nestjs/swagger';
import { SwaggerTag } from '../environment/swagger.const';
import { UserService } from './user.service';
import * as  mongodb from 'mongodb';
import { User } from '../auth/entity/user.entity';

@Controller('user')
export class UserController {
    constructor(
        private userService: UserService,
        private authService: AuthService,
    ) {

    }

    @ApiUseTags(SwaggerTag.user)
    @Post('register')
    async register(@Body() userDto: UserDto) {
        const existedUser = await this.userService.findOneByEmail(userDto.email);
        if (!existedUser) {
            const user = new User();
            user.email = userDto.email;
            user.passwordHash = await this.authService.generateHash(userDto.password);
            return HttpCode(200);
        } else {
            throw new HttpException('Email not available', HttpStatus.BAD_REQUEST);
        }
    }

}
