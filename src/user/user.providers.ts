import { Connection, Repository } from 'typeorm';
import { REPO, DBCONFIG } from '../database/repository.const';
import { User } from '../auth/entity/user.entity';

export const userProviders = [
  {
    provide: REPO.USER_REPOSITORY,
    useFactory: (connection: Connection) => connection.getRepository(User),
    inject: [DBCONFIG.DATABASE_CONNECTION],
  },

];
