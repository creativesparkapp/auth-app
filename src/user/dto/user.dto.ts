import { ApiModelProperty } from '@nestjs/swagger';

export class UserDto {

    @ApiModelProperty()
    email: string;

    @ApiModelProperty()
    password: string;

}
