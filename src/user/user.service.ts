import { Injectable, Inject } from '@nestjs/common';
import { Repository, InsertResult, ObjectID } from 'typeorm';
import * as  mongodb from 'mongodb';
import { REPO } from '../database/repository.const';
import { User } from '../auth/entity/user.entity';

export enum UserRole {
  admin = 'admin',
}

@Injectable()
export class UserService {

  constructor(
    @Inject(REPO.USER_REPOSITORY)
    private readonly userRepository: Repository<User>,

  ) { }

  async findOneByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({ email });
  }
  async findOneById(id: ObjectID): Promise<User | undefined> {
    return await this.userRepository.findOne(new mongodb.ObjectId(id));
  }
  async createUser(user: User): Promise<User | undefined> {
    user.createdOn = new Date();
    return await this.userRepository.save(user);
  }
}
