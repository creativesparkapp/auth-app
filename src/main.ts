import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AuthModule } from './auth/auth.module';
import { SwaggerTag } from './environment/swagger.const';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle('setel-fullstack (AUTH)')
    .setDescription('Microservice for auth module')
    .setVersion('DEMO')
    .addBearerAuth('authorization')
    .addTag(SwaggerTag.auth)
    .build();
  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);
  await app.listen(3001);
}
bootstrap();
