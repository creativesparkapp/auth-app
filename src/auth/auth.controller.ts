import { Controller, UseGuards, Post, Request, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { SwaggerTag } from '../environment/swagger.const';
import { LoginDto } from './dto/auth.dto';

@ApiUseTags(SwaggerTag.auth)
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @UseGuards(AuthGuard('local'))
    @Post('login')
    async login(@Body() loginDto: LoginDto) {
        return this.authService.generateToken(loginDto);
    }
}
