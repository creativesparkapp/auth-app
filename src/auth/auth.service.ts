import { Injectable, Inject, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import * as  bcrypt from 'bcrypt';
import { UserService } from '../user/user.service';
import { LoginDto } from './dto/auth.dto';
import { JwtPayloadDto } from './dto/jwt-payload.dto';
import { User } from './entity/user.entity';
import { REPO } from '../database/repository.const';

@Injectable()
export class AuthService {
  constructor(
    @Inject(REPO.USER_REPOSITORY)
    private readonly userRepository: Repository<User>,
    private readonly usersService: UserService,
    private readonly jwtService: JwtService,
  ) { }

  async validateUser(email, password): Promise<any> {
    const user = await this.usersService.findOneByEmail(email);
    if (user && await this.validateHash(password, user.passwordHash)) {
      return user;
    }
    return null;
  }

  async generateHash(password) {
    const hash = await bcrypt.hash(password, 10).then(generatedHash => {
      return generatedHash;
    });
    return hash;
  }

  async validateHash(password, passHash) {
    const match = await new Promise((resolve, reject) => {
      bcrypt.compare(password, passHash, (err, res) => {
        if (err) { reject(err); }
        resolve(res);
      });
    });
    return match;
  }

  async generateToken(user: LoginDto) {
    const userData = await this.usersService.findOneByEmail(user.email);
    if (userData) {
      // const payload = new JwtPayloadDto();
      // payload.uid = userData.id;

      return {
        access_token: this.jwtService.sign({ uid: userData.id }),
      };
    } else {
      return new UnauthorizedException();
    }
  }
}
