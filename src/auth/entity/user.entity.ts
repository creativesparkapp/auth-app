import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';

export enum AdminType {
    SUPER,
    NORMAL,
}

@Entity()
export class User {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    email: string;

    @Column()
    verified: string;

    @Column()
    isActive: boolean;

    @Column()
    roles: AdminType[];

    @Column()
    passwordHash: string;

    @Column()
    createdOn: Date;
}
