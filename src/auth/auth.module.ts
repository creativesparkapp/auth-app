import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LocalStrategy } from './stratergy/local.strategy';
import { JwtStrategy } from './stratergy/jwt.strategy';
import { UsersModule } from '../user/user.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { AuthController } from './auth.controller';
import { DatabaseModule } from '../database/database.module';
import { userProviders } from '../user/user.providers';

@Module({
  imports: [
    DatabaseModule,
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1days' },
    }),
  ],
  providers: [AuthService, LocalStrategy, ...userProviders, JwtStrategy],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule { }
