import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './user/user.module';
import { AuthController } from './auth/auth.controller';
import { DatabaseModule } from './database/database.module';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [AuthModule, UsersModule, DatabaseModule, PassportModule],
  controllers: [AuthController],
  providers: [AppService],
})
export class AppModule { }
